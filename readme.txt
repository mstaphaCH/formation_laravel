Formation Laravel  
Set up a MySQL DB named 'shop-menu' and import install.sql file into it, make sure you edit credentials in app/config/database.php to match yours
Open up terminal and CD into the folder of this repo
Run "php artisan serve" to run the application
Open up the browser and navigate to "localhost:8000" to see it in action